<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property int $frequency
 * @property string $name
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

public static function findAllByName($name) //קוארי מתקבל מהקונטרולר שמתקבל מהיו אר אל
{

    return Tag::find()->where(['like','name','$name'])->limit(50)->all();//' , עד 50 תגיות ,לייק הוא בוליאני אמת או שקר במידה והסטרינד מכיל את התווים
}



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'frequency' => 'Frequency',
            'name' => 'Name',
        ];
    }
}
