<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;

/**
 * ArticleSearch represents the model behind the search form of `app\models\Article`.
 */
class ArticleSearch extends Article
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'editor_id', 'category_id', 'created_by', 'updated_by'], 'integer'],
            [['title', 'descriptin', 'body', 'created_at', 'updated_at','tag'], 'safe'],
        ];
    }
public $tag;
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)//מקבלת פרמטר מיו אר אל
    {
        $query = Article::find();//כמו סלקט כוכבית מארטיקל

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([//, יפעל כאשר הגיע משהו ביו אר אל ואם הגיע נל אין פילטר, לוקח את הקוארי ומוסיף לו תנאים
            'id' => $this->id,
            'author_id' => $this->author_id,
            'editor_id' => $this->editor_id,
            'category_id' => $this->category_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'descriptin', $this->descriptin])
            ->andFilterWhere(['like', 'body', $this->body]);

        //fdd tags condition
        if(!empty($this->tag)){

           $condision = Tag::find()->select('id')->where(['IN','name',$this->tag]);
           $query->joinWith('tags'); //here is the condition
           $query->andWhere(['IN','tag_id',$condision]);
        }

        return $dataProvider;
    }
}
