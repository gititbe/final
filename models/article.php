<?php

namespace app\models;

use Yii;
use dosamigos\taggable\Taggable;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $descriptin
 * @property string $body
 * @property int $author_id
 * @property int $editor_id
 * @property int $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }
public function behaviors()
{
    return [
        // for different configurations, please see the code
        // we have created tables and relationship in order to
        // use defaults settings
        Taggable::className(),//בעזרת שורה זו משתמשים במחלקה טאג, אי אפשר לרשת יותר ממחלקה אחת לכן נשמש בזה
          BlameableBehavior::className(),     
    ];
}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['tagNames'], 'safe'],
            [['author_id', 'editor_id', 'category_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],//מוגן
            [['title', 'descriptin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'descriptin' => 'Descriptin',
            'body' => 'Body',
            'author_id' => 'Author ID',
            'editor_id' => 'Editor ID',
            'category_id' => 'Category',//הורדנו את הID 
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    public function getCategory(){
        return $this->hasOne(Category::className(),['id'=>'category_id']); //פונקציית גט מחברת בין טבלאות, ו 
    }
    
    public function getTags()
{
    return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_tag_assn', ['article_id' => 'id']);
}
}
