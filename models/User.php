<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $username
 * @property string $auth_key
 * @property string $password
 * @property string $created_at
 * @property string $updated_at
 * @property int $crated_by
 * @property int $updated_by
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['crated_by', 'updated_by'], 'integer'],
            [['name', 'email', 'username', 'auth_key', 'password'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }
////////////////////////////////////27.5.18//////////////////////////
      public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
     //   return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]);
    }

    public function validatepassword($password){
    return \Yii::$app->security->validatepassword($password,$this->password);
    }


   public function beforesave($insert){//לכל מחלקה יש ביפור סייב שנבנה עם המחלקה, כרגע אנו דורסים את הביפור סייב המקורי
       if(parent::beforeSave($insert)){//מופעלת לפני השמירה באקטיב רקורד
        if ($this->isNewRecord){
            $this->auth_key = \Yii::$app->security->generateRandomString();
        }
        if($this->isAttributeChanged('password')){
            $this->password =\Yii::$app->security->generatePasswordHash($this->password);
        }
        return true;
       }
     return false;
   }
    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password' => 'Password',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'crated_by' => 'Crated By',
            'updated_by' => 'Updated By',
        ];
    }

    
}
