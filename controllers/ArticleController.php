<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
 use yii\filters\AccessControl;
/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    
public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),//כנראה שצריך יוז
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update'],//החוקים חלים רק על אפדייט
                'rules' => [
                    [
                        'allow' => true,//לאפשר לעשות אפדייט
                        'actions' => ['update'],//למי שיש את ההרשאה אפדייט
                        'roles' => ['updateArticle'],
                        'roleParams' => function() {
                            return ['article' => Article::findOne(['id' => Yii::$app->request->get('id')])];//מעביר לחוק מידע באיזה ארטיקל מדובר
                        },
                    ],
                    
                ],
            ],
        ];            
    }


    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);//כל הפרמטרים שנמצאים ביו אר אל

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
     $model=$this->findModel($id);//נשלוף את המאמר לפי איי די
    $tagModels = $model->tags;//נשלוף את כל התגים של המאמר
    
    $tags='';//הטאגס הוא האיץטיאמאל שהתקבל
    foreach($tagModels as $tag){
    $tagLink =Html::a($tag->name,['article/index','ArticleSearch[tag]' => $tag->name]);//מהתאג נקח את התכונה ניימ //'article/index לך לקונטרולר ארטיקל ותפעיל עליוינדקס
    $tags.=','.$tagLink;
   
    }
        $tags=substr($tags,1);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'tags' => $tags,
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        
        $model = $this->findModel($id);

        //if (\Yii::$app->user->can('updateArticle',['article'=>$model])) {

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
   // }
   // throw new NotFoundHttpException('soory this isnt your article.');
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
