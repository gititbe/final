<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $rule = new \app\rbac\AuthorRule;//אנחנו יצאנו את אוטורול והיא נמצאית באר בי אנ סי
        $auth->add($rule);
       
    }
}