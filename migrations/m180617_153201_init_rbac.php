<?php

use yii\db\Migration;

/**
 * Class m180617_153201_init_rbac
 */
class m180617_153201_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
     $auth = Yii::$app->authManager;   //מאכלסים את הטבלאות באמצעות מיגריישן ולא ידניט
      $author = $auth->createRole('author');//יצירת תפקיד
      $auth->add($author);

      $admin = $auth->createRole('admin');
      $auth->add($admin);
              
      $auth->addChild($admin, $author);//אבא של אוטור הוא אדמין

      $manageUsers = $auth->createPermission('manageUsers');
      $auth->add($manageUsers);//אד מוסיף לדטה בייס

      $updateArticle = $auth->createPermission('updateArticle');
      $auth->add($updateArticle);                    
              
      $updateOwnArticle = $auth->createPermission('updateOwnArticle');

      $rule = new \app\rbac\AuthorRule;
      $auth->add($rule);
              
      $updateOwnArticle->ruleName = $rule->name;                
      $auth->add($updateOwnArticle);                 
                                
              
      $auth->addChild($admin, $manageUsers);
      $auth->addChild($updateOwnArticle, $updateArticle);
      $auth->addChild($author, $updateOwnArticle); 

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180617_153201_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180617_153201_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
