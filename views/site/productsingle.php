<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Product;
use yii\widgets\DetailView;

$this->title = 'Product Single';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="site-productsingle">
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',               
            'body',
        ],
    ]);
    
    
    ?> 


    <code><?= __FILE__ ?></code>
</div>